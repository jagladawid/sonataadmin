<?php

namespace Acme\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity
 */
class Book
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORm\Column(name="name", type="string",length=255, unique=false, nullable=false)
    */

    protected $name;

    /**
     * @var float $price
     * @ORm\Column(name="price", type="decimal", precision=9, scale=2, unique=false, nullable=false)
    */

    protected $price;


    /*
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection $authors
     *
     * @ORM\ManyToMany(targetEntity="Acme\BookBundle\Entity\Author", inversedBy="books")
     * @ORM\JoinTable(name="author_book",
     *  joinColumns={@ORM\JoinColumn(name="book_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="author_id", referencedColumnName="id")}
     *)
     */
     
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection $authors
     * @ORm\ManyToMany(targetEntity="Acme\BookBundle\Entity\Author",mappedBy="books")
    */

    protected $authors;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authors = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @return string
    */

    public function __toString()
    {
        return $this->getName()." ";
    }

    
    /**
     * Set name
     *
     * @param string $name
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Book
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add authors
     *
     * @param \Acme\BookBundle\Entity\Author $authors
     * @return Book
     */
    public function addAuthor(\Acme\BookBundle\Entity\Author $authors)
    {
        $this->authors[] = $authors;
    
        return $this;
    }

    /**
     * Remove authors
     *
     * @param \Acme\BookBundle\Entity\Author $authors
     */
    public function removeAuthor(\Acme\BookBundle\Entity\Author $authors)
    {
        $this->authors->removeElement($authors);
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}